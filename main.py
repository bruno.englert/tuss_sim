import math

import arcade
import numpy as np
import scipy
import scipy.linalg
import matplotlib.pyplot as plt

from utils.create_bridge import create_bridge
from utils.plot_bridge import plot_bridge
from utils.get_stiffness_mtx import get_stiffness_mtx


import arcade

# Set up the constants
SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 600


class Line:
    """ Class to represent a rectangle on the screen """

    def __init__(self, x0, y0, x1, y1):
        """ Initialize our rectangle variables """

        # Position
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1

    def draw(self):
        """ Draw our rectangle """
        vector_x = self.x0 - self.x1
        vector_y = self.y0 - self.y1
        length = math.sqrt(vector_x * vector_x + vector_y * vector_y)
        if length != 0:
            arcade.draw_line(self.x0, self.y0, self.x1, self.y1, color=arcade.color.RED, line_width=3)


class Tuss:
    def __init__(self, nSpan, nFloor):
        self.nodes, self.elements = create_bridge(20, 5)
        self.K = get_stiffness_mtx(self.nodes, self.elements)
        self.restricted_nodes = np.asarray([0, 1, 38, 39])
        self.actDof = np.setdiff1d(np.arange(self.nodes.shape[0] * 2), self.restricted_nodes)
        self.K = self.K[np.ix_(self.actDof, self.actDof)]

        self.P, self.L, self.U = scipy.linalg.lu(self.K)
        self.F = np.zeros((self.nodes.shape[0] * 2), dtype=np.float64)
        self.displacement = np.zeros((self.nodes.shape[0], 2), dtype=np.float64)

        self.PL_inv = np.linalg.inv(np.matmul(self.P, self.L))
        self.U_inv = np.linalg.inv(self.U)

    def update_load_F(self, fx, fy):
        scale = 5.
        x_idx = [i for i in range(74 * 2, (74 + 16) * 2, 2)]
        self.F[x_idx] = fx * scale
        y_idx = [i for i in range(74 * 2 + 1, (74 + 16) * 2 + 1, 2)]
        self.F[y_idx] = fy * scale
        y = np.matmul(self.PL_inv, np.matmul(self.P, self.F[self.actDof]))
        U_active = np.matmul(self.U_inv, y)

        U = np.zeros(self.nodes.shape[0] * 2)
        U[np.ix_(self.actDof)] = U_active
        self.displacement = U.reshape((-1, 2))

    def draw(self):
        no_elements = self.elements.shape[0]
        no_nodes = self.nodes.shape[0]

        elements = self.elements
        nodes = self.nodes + self.displacement

        scale=60.
        shift=(10, 100)
        color=arcade.color.BLACK

        for i in range(no_elements):
            pt1 = elements[i][0]
            pt2 = elements[i][1]
            arcade.draw_line(nodes[pt1][0] * scale + shift[0], nodes[pt1][1] * scale + shift[1],
                             nodes[pt2][0] * scale + shift[0], nodes[pt2][1] * scale + shift[1], color, 2)


class MyGame(arcade.Window):
    """ Main application class. """
    def __init__(self, width, height):
        super().__init__(width, height, title="Tuss simulation")
        arcade.set_background_color(arcade.color.WHITE)
        self.line = None
        self.tuss = None
        self.left_down = False

    def setup(self):
        """ Set up the game and initialize the variables. """
        self.line = Line(0, 0, 0, 1)
        self.tuss = Tuss(20,5)
        self.left_down = False

    def update(self, dt):
        """ Move everything """
        pass

    def on_draw(self):
        """
        Render the screen.
        """
        arcade.start_render()

        self.tuss.draw()
        self.line.draw()

    def on_mouse_motion(self, x, y, dx, dy):
        """
        Called whenever the mouse moves.
        """
        if not self.left_down:
            self.line.x0 = x
            self.line.y0 = y
        else:
            self.tuss.update_load_F(self.line.x1-self.line.x0, self.line.y1-self.line.y0)

        self.line.x1 = x
        self.line.y1 = y

    def on_mouse_press(self, x, y, button, modifiers):
        """
        Called when the user presses a mouse button.
        """
        if button == arcade.MOUSE_BUTTON_LEFT:
            self.left_down = True

    def on_mouse_release(self, x, y, button, modifiers):
        """
        Called when a user releases a mouse button.
        """
        if button == arcade.MOUSE_BUTTON_LEFT:
            self.left_down = False
            self.tuss.update_load_F(0, 0)

if __name__ == "__main__":
    window = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    window.setup()
    arcade.run()
