import numpy as np


def get_rotation_mtx(x, y):
    hypot = np.sqrt(x*x + y*y)
    rv = [[x/hypot, y/hypot, 0, 0],
          [0, 0, x/hypot, y/hypot]]
    return np.asarray(rv, dtype=np.float64)